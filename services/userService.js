const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    search(search) {
        const item = UserRepository.getOne({email:search.email, password:search.password});
        if(!item) {
            return null;
        }
        return item;
    }
    add(data){
        let check = this.checkData(data);
        if(check!==null) return check;
        return UserRepository.create(data);
    }
    get(id){
        return UserRepository.getOne({id:id});
    }
    getAll(){
        return UserRepository.getAll();
    }
    delete(id){
        return UserRepository.delete(id);
    }
    update(id, data){
        let check = this.checkData(data);
        if(check!==null) return check;
        return UserRepository.update(id, data);
    }
    getEmails(){
        return UserRepository.getValues(user=>user.email);
    }
    getPhones(){
        return UserRepository.getValues(user=>user.phoneNumber);
    }
    checkData(data){
        if(data.email)
        for(let i of this.getEmails()){
            if(i===data.email) return "Such email already registered";
        }
        if(data.phoneNumber);
        for(let i of this.getPhones()){
            if(i===data.phoneNumber) return "Such phone number already registered";
        }
        return null;
    }
}

module.exports = new UserService();