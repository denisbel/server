const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    add(data){
        let check = this.checkData(data);
        if(check!==null) return check;
        return FighterRepository.create(data);
    }
    getNames(){
        return FighterRepository.getValues(fighter=>fighter.name);
    }
    checkData(data){
        if(data.name)
        for(let i of this.getNames()){
            if(i===data.name) return "Such name already registered";
        }
        return null;
    }

    getAll(){
        return FighterRepository.getAll();
    }
    get(id){
        return FighterRepository.getOne({id:id});
    }

    delete(id){
        return FighterRepository.delete(id);
    }

    update(id, data){
        let check = this.checkData(data);
        if(check!==null) return check;
        return FighterRepository.update(id, data);
    }
}

module.exports = new FighterService();