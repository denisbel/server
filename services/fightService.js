const {
    FightRepository
} = require('../repositories/fightRepository');
const fighterService = require('./fighterService');

class FightsService {
    getFight(fighter1Id, fighter2Id) {
        let fighter1 = fighterService.get(fighter1Id);
        let fighter2 = fighterService.get(fighter2Id);
        if (!fighter1 || !fighter2) return null;
        let result = this.getResult(fighter1, fighter2);
        this.add(result);
        return result;
    }

    add(fight) {
        return FightRepository.create(fight);
    }

    getResult(fighter1, fighter2) {
        let HP1 = fighter1.health,
            HP2 = fighter2.health;
        let result = {
            fighter1: fighter1.id,
            fighter2: fighter2.id,
            log: []
        };
        let roll = this.getRandomArbitrary(0, 2);
        while (HP1 > 0 && HP2 > 0) {
            let fighter1Attack = this.getHitPower(fighter1),
                fighter2Attack = this.getHitPower(fighter2),
                fighter1Defense = this.getBlockPower(fighter1),
                fighter2Defense = this.getBlockPower(fighter2),
                fighter1Damage = fighter1Attack > fighter2Defense ? fighter1Attack - fighter2Defense : 0,
                fighter2Damage = fighter2Attack > fighter1Defense ? fighter2Attack - fighter1Defense : 0;
            if (roll > 1) {
                result.log.push(`fighter1Shot: ${fighter1Attack}`);
                result.log.push(`fighter2HP: ${HP2-fighter1Damage}`);
                HP2 = HP2-fighter1Damage;
                if (HP2 < 0) break;
                result.log.push(`fighter2Shot: ${fighter2Attack}`);
                result.log.push(`fighter1HP: ${HP1-fighter2Damage}`);
                HP1 = HP1-fighter2Damage;
            } else {
                result.log.push(`fighter2Shot: ${fighter2Attack}`);
                result.log.push(`fighter1HP: ${HP1-fighter2Damage}`);
                HP1 = HP1-fighter2Damage;
                if (HP1 < 0) break;
                result.log.push(`fighter1Shot: ${fighter1Attack}`);
                result.log.push(`fighter2HP: ${HP2-fighter1Damage}`);
                HP2 = HP2-fighter1Damage;
                
            }
        }
        if (HP1 > HP2) {
            result.winner = fighter1.id;
            result.looser = fighter2.id;
        } else {
            result.winner = fighter2.id;
            result.looser = fighter1.id;
        }
        return result;
    }

    getHitPower(fighter) {
        const crtiticalChance = this.getRandomArbitrary(1, 2);
        return fighter.power * crtiticalChance;
        // return hit power
    }

    getBlockPower(fighter) {
        const dodgeChance = this.getRandomArbitrary(1, 2);
        return fighter.defense * dodgeChance;
        // return block power
    }

    getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    getAll() {
        return FightRepository.getAll();
    }

    get(id) {
        return FightRepository.getOne({
            id: id
        });
    }
    // OPTIONAL TODO: Implement methods to work with fights
}

module.exports = new FightsService();