const {
    fighter
} = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    let data = req.body;
    if (Object.entries(data).length < 3) {
        res.error = {
            message: "Blank field",
            code: 400
        }
        next();
        return;
    }
    if(data.health===undefined){
        data.health=100;
    }
    let reqError = checkFighter(data);;
    if (reqError !== null) {
        res.error = reqError;
    }
    next();
}

const updateFighterValid = (req, res, next) => {
      // TODO: Implement validatior for user entity during update
      let data = req.body;
      if (Object.entries(data).length < 1) {
          res.error = {
              message: "No properties",
              code: 400
          }
          next();
          return;
      }
      let reqError = checkFighter(data, true);
      if (reqError !== null) {
          if(reqError!=="Lack of properties") res.error = reqError;
      }
      next();
}

function checkFighter(data, isUpdate=false) {
    let properties = {
        id: false,
        name: false,
        health: false,
        defense: false,
        power: false

    },
    notExist = true;
    loop: for (const [key, value] of Object.entries(data)) {
        notExist = true;
        for (const [keyFighter, valueFighter] of Object.entries(fighter)) {
            if (key === "id") {
                properties.id = true;
                break loop;
            }
            if (key === keyFighter) {
                notExist = false;
                properties[key] = true;
                break;
            }
        }
        if (notExist) break;
    }
    if (properties.id || notExist) {
        return {
            message: "Invalid property",
            code: 400
        }
    }
    if ((!properties.name || !properties.defense || !properties.power)&&!isUpdate) return {
        message: "Lack of properties",
        code: 400
    }
    if(properties.power&&(data.power<1||data.power>100||typeof data.power !="number")) return{
        message: "Invalid power",
        code: 400
    }
    if(properties.defense&&(data.defense<1||data.defense>10||typeof data.defense !="number")) return{
        message: "Invalid defense",
        code: 400
    }
    if(properties.health&&(data.health<80||data.health>120||typeof data.health != "number"))return{
        message: "Invalid health",
        code: 400
    }
    return null;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;