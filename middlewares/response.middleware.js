const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if (res.error) {
        res.status(res.error.code);
        res.json({
            error: true,
            message: res.error.message
        });
    } 
    else {
        res.status(200);
        res.result
        if(res.result) res.json(res.result);
        else res.json({
            error: false,
            message: "OK"
        });
    }
    next();
}

exports.responseMiddleware = responseMiddleware;