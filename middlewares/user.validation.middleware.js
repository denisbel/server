const {
    user
} = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation (validate properies )
    let data = req.body;
    if (Object.entries(data).length < 5) {
        res.error = {
            message: "Blank field",
            code: 400
        }
        next();

        return;
    }
    let reqError = checkUser(data);
    if (reqError !== null) {
        res.error = reqError;
    }
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    let data = req.body;
    if (Object.entries(data).length < 1) {
        res.error = {
            message: "No fields",
            code: 400
        }
        next();
        return;
    }
    let reqError = checkUser(data);
    if (reqError !== null) {
        res.error = reqError;
    }
    next();
}

function checkUser(data) {
    for (const [key, value] of Object.entries(data)) {
        let notExist = true;
        for (const [keyUser, valueUser] of Object.entries(user)) {
            if (key === "id") {
                notExist = true;
                break;
            }
            if (key === keyUser) {
                notExist = false;
                break;
            }
        }
        if (notExist) return {
            message: "Incorrect property",
            code: 400
        };
        if (!value || value[0] == ' ') {
            return {
                message: "Blank field",
                code: 400
            }

        }
    }
    let email = /[\w\W]+@gmail.com$/;
    if (data.email && !email.test(data.email)) {
        return {
            message: "Invalid email. Only gmail valid",
            code: 400
        }

    }
    let phone = /[+]380[0-9]\d{8}$/
    if (data.phoneNumber && !phone.test(data.phoneNumber)) {
        return {
            message: "Invalid phone. Valid: +380xxxxxxxxx",
            code: 400
        }

    }
    if (data.password && (data.password.length < 3 || typeof data.password!== "string"))
        return {
            message: "Invalid password",
            code: 400
        }
    return null;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;