const { BaseRepository } = require('./baseRepository');
const { FighterRepository } = require("./fighterRepository");

class FightRepository extends BaseRepository {
    constructor() {
        super('fights');
    }
}

exports.FightRepository = new FightRepository();