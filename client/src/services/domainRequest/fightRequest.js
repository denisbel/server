import { post } from "../requestHelper";


export const newFight = async (id1, id2) => {
    return await post(`fights/${id1}/${id2}`);
}