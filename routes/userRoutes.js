const {
    Router
} = require('express');
const UserService = require('../services/userService');
const {
    createUserValid,
    updateUserValid
} = require('../middlewares/user.validation.middleware');
const {
    responseMiddleware
} = require('../middlewares/response.middleware');

const router = Router();
router.post('/', createUserValid, (req, res, next) => {
    try {
        if (!res.error) {
            let result = UserService.add(req.body);
            if (typeof result === "string") {
                res.error = {
                    message: result,
                    code: 400
                }
            }
            res.result = result;
        }

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware, );

router.get('/:id', (req, res, next) => {
    try {
        let a = UserService.get(req.params.id);
        if (!a) {
            res.error = {
                message: "Couldn't find user",
                code: 404
            }
        } else {
            res.result = a;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        let a = UserService.getAll();
        if (!a.length) {
            res.error = {
                message: "There isn't any user",
                code: 404
            }
        } else {
            res.result = a;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {

        let a = UserService.delete(req.params.id);
        if (!a.length) {
            res.error = {
                message: `There isn't user with such id`,
                code: 404
            }
        } else {
            res.result = a;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
            if (!res.error) {
                let result = UserService.update(req.params.id, req.body);
                if (typeof result === "string") {
                    res.error = {
                        message: result,
                        code: 400
                    }
                }
                res.result = result;
            }

            
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);
// TODO: Implement route controllers for user

module.exports = router;