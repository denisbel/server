const { Router } = require('express');
const FightsService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

router.post('/:id1/:id2', (req, res, next) => {
    try {
        let a = FightsService.getFight(req.params.id1,req.params.id2);
        if (!a) {
            res.error = {
                message: "Fighter not found",
                code: 404
            }
        } else {
            res.result = a;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        let a = FightsService.getAll();
        if (!a.length) {
            res.error = {
                message: "There isn't any fights",
                code: 404
            }
        } else {
            res.result = a;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        let a = FightsService.get(req.params.id);
        if (!a) {
            res.error = {
                message: "Fight not found",
                code: 404
            }
        } else {
            res.result = a;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);
// OPTIONAL TODO: Implement route controller for fights

module.exports = router;