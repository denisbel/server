const {
    Router
} = require('express');
const FighterService = require('../services/fighterService');
const {
    responseMiddleware
} = require('../middlewares/response.middleware');
const {
    createFighterValid,
    updateFighterValid
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.post('/', createFighterValid, (req, res, next) => {
    try {
        if (!res.error) {
            let result = FighterService.add(req.body);
            if (typeof result === "string") {
                res.error = {
                    message: result,
                    code: 400
                }
            }
            res.result = result;
        }


    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware, );

router.get('/', (req, res, next) => {
    try {
        let a = FighterService.getAll();
        if (!a.length) {
            res.error = {
                message: "There isn't any fighter",
                code: 404
            }
        } else {
            res.result = a;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        let a = FighterService.get(req.params.id);
        if (!a) {
            res.error = {
                message: "Fighter not found",
                code: 404
            }
        } else {
            res.result = a;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {

        let a = FighterService.delete(req.params.id);
        if (!a.length) {
            res.error = {
                message: `There isn't fighter with such id`,
                code: 404
            }
        } else {
            res.result = a;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
            if (!res.error) {
                let result = FighterService.update(req.params.id, req.body);
                if (typeof result === "string") {
                    res.error = {
                        message: result,
                        code: 400
                    }
                }
                res.result = result;
            }

            
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

// TODO: Implement route controllers for fighter

module.exports = router;