const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        let result = AuthService.login(req.body);

        // TODO: Implement login action (get the user if it exist with entered credentials)
        res.data = result;
    } catch (err) {
        res.error = {
            message: "Not valid user",
            code: 400
        }
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;